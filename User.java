package covid.tracker;

//import covid.tracker.CloseContact;
import java.util.ArrayList;

public class User {

	private String ppsn;
	private String name;
	private String phone;

	private ArrayList<CloseContact> closeContactArray = new ArrayList<CloseContact>();

	private int numberOfUsers = 0;

	public String contactName;
	public String contactPhone;
	public String printMessage;

	public User() {
	}

	public User(String ppsn, String name, String phone) {
		this.ppsn = ppsn;
		this.name = name;
		this.phone = phone;
		numberOfUsers++;
	}

	public void addCloseContact(String contactName, String contactPhone) {
		CloseContact closeContact = new CloseContact(contactName, contactPhone);
		closeContactArray.add(closeContact);
		printMessage = "New close contact: " + contactName + " (phone: " + contactPhone + ") has been added.";
		System.out.println(printMessage);
	}

	public int getTotalContactCount() {
		int totalCount = closeContactArray.size();
		return totalCount;
	}

	public CloseContact getCloseContact(int pos) {
		CloseContact contactPos = closeContactArray.get(pos);
		return contactPos;
	}

	public String getPpsn() {
		return ppsn;
	}

	public void setPpsn(String ppsn) {
		this.ppsn = ppsn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getNumberOfUsers() {
		return numberOfUsers;
	}

	public void setNumberOfUsers(int numberOfUsers) {
		this.numberOfUsers = numberOfUsers;
	}

	public ArrayList<CloseContact> getCloseContactArray() {
		return closeContactArray;
	}

	public void setCloseContactArray(ArrayList<CloseContact> closeContactArray) {
		this.closeContactArray = closeContactArray;
	}

	@Override
	public String toString() {
		return "PPSN=" + ppsn + ", Name=" + name + ", Phone=" + phone;
	}

}
