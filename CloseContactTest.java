package covid.tracker;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class CloseContactTest {
	public CloseContact sut = new CloseContact("John", "0871234567");
	public CloseContact sut2 = new CloseContact();

	@Test
	public void testConstructor() {
		assertEquals("John", sut.getName());
	}

	@Test
	public void testName() {
		sut2.setName("Jack");
		assertEquals("Jack", sut2.getName());
	}

	@Test
	public void testPhone() {
		sut2.setPhone("353836655445");
		assertEquals("353836655445", sut2.getPhone());
	}

}
