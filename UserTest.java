package covid.tracker;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

public class UserTest {

	public User sut = new User();
	public User sut2 = new User("1234567W", "Niall", "0876639065");

	@Test
	public void testConstructor() {
		assertEquals("Niall", sut2.getName());
		assertEquals("1234567W", sut2.getPpsn());
		assertEquals("0876639065", sut2.getPhone());
	}

	@Test
	public void testPpsn() {
		sut.setPpsn("5556567d");
		assertEquals("5556567d", sut.getPpsn());
	}

	@Test
	public void testName() {
		sut.setName("Owen");
		assertEquals("Owen", sut.getName());
	}

	@Test
	public void testPhone() {
		sut.setPhone("0456755334");
		assertEquals("0456755334", sut.getPhone());
	}

	@Test
	public void testNumberOfUsers() {
		sut.setNumberOfUsers(5);
		assertEquals(5, sut.getNumberOfUsers());
	}

	@Test
	public void testCloseContactArray() {
		ArrayList<CloseContact> newArray = new ArrayList<CloseContact>();
		CloseContact newContact = new CloseContact();
		newArray.add(newContact);
		sut.setCloseContactArray(newArray);
		assertEquals(newArray, sut.getCloseContactArray());
	}

	@Test
	public void testAddCloseContact() {
		sut.addCloseContact("Sean", "0857777888");
		assertEquals("New close contact: Sean (phone: 0857777888) has been added.", sut.printMessage);
	}

	@Test
	public void testGetTotalContactCount() {
		sut.addCloseContact("Sean", "0857777888");
		sut.addCloseContact("Shaun", "0857734636");
		sut.addCloseContact("Senan", "0876573478");
		assertEquals(3, sut.getTotalContactCount());
	}

	@Test
	public void testGetCloseContact() {
		sut.addCloseContact("Joanne", "0857777888");
		sut.addCloseContact("Shaun", "0857734636");
		sut.addCloseContact("Rob", "0876573478");
		assertEquals("Shaun", sut.getCloseContact(1).getName());
		assertEquals("0857734636", sut.getCloseContact(1).getPhone());
	}

	@Test
	public void testToString() {
		assertEquals("PPSN=1234567W, Name=Niall, Phone=0876639065", sut2.toString());
	}

}
