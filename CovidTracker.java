package covid.tracker;

import java.util.ArrayList;
import java.util.Scanner;

public class CovidTracker {

	public static String welcomeSelection;
	public static String menuSelection;
	public static boolean loopMenu = false;
	public static Scanner sc = new Scanner(System.in);
	public String welcomeMessage;
	public ArrayList<User> userArray = new ArrayList<User>();

	public static void main(String[] args) {

		CovidTracker covidTracker = new CovidTracker();
		covidTracker.welcomeMenu();//Display at app start up only

		while (loopMenu = true) {//return to main menu after each function

			covidTracker.mainMenu();//display main menu 

			if (menuSelection.equalsIgnoreCase("x")) {//exit application
				System.out.println("Goodbye!");
				return;
			}//menu functions dependent on input from mainMenu()
			covidTracker.menuOne();//these methods carry out menu functions
			covidTracker.menuTwo();
			covidTracker.menuThree();
			covidTracker.menuFour();
			covidTracker.menuFive();
			covidTracker.menuSix();
			covidTracker.menuIncorrectInput();

		}
	}

	public CovidTracker() {// empty constructor
	}

	public void welcomeMenu() {// method for Welcome menu
		welcomeMessage = "Welcome to the Covid Tracker App. Press any key for the Main Menu.";
		System.out.println(welcomeMessage);
		welcomeSelection = sc.nextLine();
		loopMenu = true;
	}

	public void mainMenu() {//Main menu display. Input for menuSelection will determine next operation 
		String line0 = "--------------------------------------------------";
		String line1 = "Main Menu. Please make a Selection: ";
		String line2 = "1. Create new user ";
		String line3 = "2. Add close contacts ";
		String line4 = "3. Daily Check in ";
		String line5 = "4. Edit user data";
		String line6 = "5. View all users (admin password required)";
		String line7 = "6. Delete user data (admin password required)";
		String line8 = "X. Exit app ";

		System.out.println(line0);
		System.out.println(line1);
		System.out.println(line2);
		System.out.println(line3);
		System.out.println(line4);
		System.out.println(line5);
		System.out.println(line6);
		System.out.println(line7);
		System.out.println(line8);
		menuSelection = sc.next();
	}

	public void menuOne() {//create new user and store in userArray<>
		if (menuSelection.equals("1")) {
			String userPPSN;
			String userName;
			String userPhone;

			System.out.println("Enter your PPSN:");
			userPPSN = sc.next();
			System.out.println("Enter your name:");
			sc.nextLine();//removes delimiter from sc.next()
			userName = sc.nextLine();
			System.out.println("Enter your phone number:");
			userPhone = sc.nextLine();

			User user = new User(userPPSN, userName, userPhone);
			userArray.add(user);
			System.out.println("User " + userName + ", PPSN: " + userPPSN + " successfully created.");
			System.out.println("Total users: " + userArray.size());
			return;
		}
	}

	public void menuTwo() {//add close contacts to closeContactArrayList<> in User class
		if (menuSelection.equals("2")) {
			String userPPSN;
			String contactName;
			String contactPhone;

			System.out.println("Enter your PPSN:");
			userPPSN = sc.next();

			boolean found = false;
			int pos = 0;
			while (pos < userArray.size() && !found) {//search for user by PPSN
				if (userPPSN.equals(userArray.get(pos).getPpsn())) {
					found = true;
					System.out.println("User PPSN found.");
					System.out.println("Enter close contact name:");
					sc.nextLine();
					contactName = sc.nextLine();
					System.out.println("Enter close contact number:");
					contactPhone = sc.nextLine();
					userArray.get(pos).addCloseContact(contactName, contactPhone);
					System.out.println("You have " + userArray.get(pos).getTotalContactCount() 
							+ " close contacts listed"); // displays number of closeContacts in ArrayList
				} else {
					pos++;
				}
			}
			if (pos >= userArray.size()) {
				System.out.println("PPSN not found");
				return;
			}
		}
	}

	public void menuThree() {//Daily covid check in. Displays closeContacts of user if they are a covid risk
		String userPPSN;

		if (menuSelection.equals("3")) {
			String abroad;
			String positiveTest;
			String symptoms;

			System.out.println("Enter your PPSN:");
			userPPSN = sc.next();

			boolean found = false;
			int pos = 0;
			while (pos < userArray.size() && !found) {
				if (userPPSN.equals(userArray.get(pos).getPpsn())) {
					found = true;
					System.out.println("User PPSN found.");
					System.out.println("Have you been abroad in the past 14 days? (y or n)");
					sc.nextLine();
					abroad = sc.nextLine();
					System.out.println(
							"Have you been in contact with anyone who has tested positive for Covid-19? (y or n)");
					positiveTest = sc.nextLine();
					System.out.println(
							"Are you suffering from fever, dry cough, fatigue, loss of taste or smell? (y or n)");
					symptoms = sc.nextLine();

					if (abroad.equals("y") || positiveTest.equals("y") || symptoms.equals("y")) {//answering "y" to any one question will determine the user must self isolate immediately
						System.out.println("WARNING! YOU MUST SELF-ISOLATE IMMEDIATELY!");
						System.out.println("If symptoms develop/persist, seek remote medical advice.");
						System.out.println("Here are your close contacts to carry out contact tracing:");

						if (userArray.get(pos).getTotalContactCount() > 0) {//prints out closeContacts from ArrayList of User class, if any
							for (int i = 0; i < userArray.get(pos).getTotalContactCount(); i++) {
								System.out.println("Contact: " + userArray.get(pos).getCloseContact(i).getName()
										+ " (Phone: " + userArray.get(pos).getCloseContact(i).getPhone() + ")");
							}
						} else {
							System.out.println("you have no close contacts entered. Goodbye");
						}
					} else {
						System.out.println("Thank you for completing your daily check in. Stay safe!");
					}
				} else {
					pos++;
				}
			}
			if (pos >= userArray.size()) {
				System.out.println("PPSN not found");
				return;
			}
		}
	}

	public void menuFour() {//Users can edit their variables
		String userPPSN;

		if (menuSelection.equals("4")) {

			System.out.println("Enter user PPSN to be edited:");
			userPPSN = sc.next();

			boolean found = false;
			int pos = 0;
			while (pos < userArray.size() && !found) {//search users by PPSN
				if (userPPSN.equals(userArray.get(pos).getPpsn())) {
					found = true;
					System.out.println("User name: " + userArray.get(pos).getName() + ", PPSN: "
							+ userArray.get(pos).getPpsn() + ", Phone: " + userArray.get(pos).getPhone());

					System.out.println("Enter updated PPSN:");
					userArray.get(pos).setPpsn(sc.next());//next input sets new PPSN value
					System.out.println("Enter updated name:");
					sc.nextLine();
					userArray.get(pos).setName(sc.nextLine());
					System.out.println("Enter updated phone number:");
					userArray.get(pos).setPhone(sc.nextLine());//next, display updated information
					System.out.println("Updated user name: " + userArray.get(pos).getName() + ", Updated PPSN: "
							+ userArray.get(pos).getPpsn() + ", Updated Phone: " + userArray.get(pos).getPhone());
				} else {
					pos++;
				}
			}
			if (pos >= userArray.size()) {
				System.out.println("PPSN not found");
				return;
			}
		}
	}

	public void menuFive() {//allows admin to view all users if they have the password
		String userPassword;
		if (menuSelection.equals("5")) {

			System.out.println("Enter admin password (hint = guest):");
			userPassword = sc.next();
			if (userPassword.equalsIgnoreCase("guest")) {//password must be correct (not case sensitive)
				if (userArray.size() > 0) {
					for (int i = 0; i < userArray.size(); i++) {
						System.out.println("User name: " + userArray.get(i).getName() + ", PPSN: "
								+ userArray.get(i).getPpsn() + ", Phone: " + userArray.get(i).getPhone());
					}
				} else {
					System.out.println("No users have been added.");
				}
			} else {
				System.out.println("Wrong password input");
			}
		}
	}

	public void menuSix() {//allows admin to delete a user
		String userPPSN;
		String userPassword;
		boolean userRemoved = false;

		if (menuSelection.equals("6")) {

			System.out.println("Enter admin password (hint = guest):");
			userPassword = sc.next();
			if (userPassword.equalsIgnoreCase("guest")) {

				System.out.println("Enter user PPSN to be deleted:");
				userPPSN = sc.next();

				userRemoved = false;
				boolean found = false;
				int pos = 0;
				while (pos < userArray.size() && !found) {
					if (userPPSN.equals(userArray.get(pos).getPpsn())) {
						found = true;
						System.out.println("User " + userArray.get(pos).getPpsn() + " (PPSN " + userPPSN
								+ ") removed successfully.");
						userArray.remove(pos);
						System.out.println("There are " + userArray.size() + " Users remaining.");//displays number of remaining users
						userRemoved = true;
					} else {
						pos++;
					}
				}
				if (pos >= userArray.size() && !userRemoved) {
					System.out.println("PPSN not found");
					return;
				}
			} else {
				System.out.println("Wrong password input");
			}
		}
	}

	public void menuIncorrectInput() {//any input other than those specified in mainMenu will generate an error message

		if (!menuSelection.equals("1") && !menuSelection.equals("2") && !menuSelection.equals("3")
				&& !menuSelection.equals("4") && !menuSelection.equals("5") && !menuSelection.equals("6")
				&& !menuSelection.equalsIgnoreCase("x")) {
			System.out.println("Input not recognised. Please try again.");
		}
	}

	public String getWelcomeSelection() {
		return welcomeSelection;
	}

	public void setWelcomeSelection(String welcomeSelection) {
		CovidTracker.welcomeSelection = welcomeSelection;
	}

	public String getMenuSelection() {
		return menuSelection;
	}

	public void setMenuSelection(String menuSelection) {
		CovidTracker.menuSelection = menuSelection;
	}

	public boolean isLoopMenu() {
		return loopMenu;
	}

	public void setLoopMenu(boolean loopMenu) {
		CovidTracker.loopMenu = loopMenu;
	}

	public String getWelcomeMessage() {
		return welcomeMessage;
	}

	public void setWelcomeMessage(String welcomeMessage) {
		this.welcomeMessage = welcomeMessage;
	}

	public ArrayList<User> getUserArray() {
		return userArray;
	}

	public void setUserArray(ArrayList<User> userArray) {
		this.userArray = userArray;
	}

}
