package covid.tracker;

public class CloseContact {
	private String name;
	private String phone;

	public CloseContact() {
	}

	public CloseContact(String name, String phone) {
		this.name = name;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
