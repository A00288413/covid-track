package covid.tracker;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import org.junit.Test;

public class CovidTrackerTest {
	public CovidTracker sut = new CovidTracker();

	@Test
	public void testLoopMenu() {
		assertFalse(sut.isLoopMenu());
	}

	@Test
	public void testWelcomeSelection() {
		sut.setWelcomeSelection("Hello there");
		assertEquals("Hello there", sut.getWelcomeSelection());
	}

	@Test
	public void testMenuSelection() {
		sut.setMenuSelection("a");
		assertEquals("a", sut.getMenuSelection());
	}

	@Test
	public void testLoopMenuSetter() {
		sut.setLoopMenu(true);
		assertTrue(sut.isLoopMenu());
	}

	@Test
	public void testWelcomeMessage() {
		sut.setWelcomeMessage("welcome");
		assertEquals("welcome", sut.getWelcomeMessage());
	}

	@Test
	public void testGetUserArray() {
		ArrayList<User> newArray = new ArrayList<User>();
		User newUser = new User();
		newArray.add(newUser);
		sut.setUserArray(newArray);
		assertEquals(newArray, sut.getUserArray());
	}

	@Test
	public void testWelcome() {// This test requires user input in the console
		sut.welcomeMenu();
		sut.setWelcomeMessage("welcome");
		assertTrue(sut.isLoopMenu());
		assertEquals("welcome", sut.getWelcomeMessage());
	}

	@Test
	public void testMainMenu() {// This test requires user input in the console
		sut.mainMenu();
		sut.setMenuSelection("option");
		assertEquals("option", sut.getMenuSelection());
	}

	@Test
	public void testMenuOne() {// This test requires user input in the console
		sut.setMenuSelection("1");
		sut.menuOne();
		assertEquals("1", sut.getMenuSelection());
	}

	@Test
	public void testMenuTwo() {// This test requires user input in the console
		sut.setMenuSelection("2");
		sut.menuTwo();
		assertEquals("2", sut.getMenuSelection());
	}

	@Test
	public void testMenuThree() {// This test requires user input in the console
		sut.setMenuSelection("3");
		sut.menuThree();
		assertEquals("3", sut.getMenuSelection());
	}

	@Test
	public void testMenuFour() {// This test requires user input in the console
		sut.setMenuSelection("4");
		sut.menuFour();
		assertEquals("4", sut.getMenuSelection());
	}

	@Test
	public void testMenuFive() {// This test requires user input in the console
		sut.setMenuSelection("5");
		sut.menuFive();
		assertEquals("5", sut.getMenuSelection());
	}

	@Test
	public void testMenuSix() {// This test requires user input in the console
		sut.setMenuSelection("6");
		sut.menuSix();
		assertEquals("6", sut.getMenuSelection());
	}

	@Test
	public void testIncorrectMenuInput() {// This test requires user input in the console
		sut.setMenuSelection("&");
		sut.menuIncorrectInput();
		assertEquals("&", sut.getMenuSelection());
	}

	@Test
	public void testExit() {// This test requires user input in the console
		sut.setMenuSelection("x");
		assertEquals("x", sut.getMenuSelection());
	}

}
